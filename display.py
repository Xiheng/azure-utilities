# Display folder size with depth=1
# Command line interactive display
from database import Session, Container, Folder, Blob

def display(account_name, container_name, prefix=""):
    sess = Session()
    container_id = sess.query(Container.id).filter_by(account_name=account_name, name=container_name).first()[0]
    root_id = sess.query(Folder.id).filter_by(container_id=container_id, path="").first()[0]
    q = sess.query(Folder).filter_by(container_id=container_id, parent_id=root_id).order_by(Folder.size.desc())[:10]
    for idx, f in enumerate(q):
        print(idx+1, f)
    

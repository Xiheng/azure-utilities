# Azure blob storage probe

一个用来探查Azure Blob容器使用情况的工具，使用Azure Python SDK逐文件夹探查blob，并使用SQLAlchemy将信息写入到数据库，最终展示各个文件夹及文件的存储空间占用。支持中断后断点继续执行。

## Modules

- main.py
  - 程序主入口
- scan.py
  - 使用DFS遍历容器，将走过的路径存入数据库，计算总大小等信息
- database.py
  - 建立数据库模型，使用SQLAlchemy ORM操纵数据库
  - 数据表：
    - container
    - folder 
    - blob
- display.py
  - 查询数据库，展示统计信息
- tools.py
  - 单位换算工具等
- secrets.json
  - 用于记录要探查容器名称，以及SAS或连接字符串。

```json
// secrets.json format
{
  "list": [
    {
      "type": "SAS",
      "account_name": "xxx",
      "container_name": "xxx",
      "base_url":"https://xxx.blob.core.windows.net/",
      "secrets": "?sv=2020-02-10&ss=b&srt=c&sp=rwdlacx&se=xxx&st=xxx&spr=https&sig=xxx"
  },
  {
      "type": "connection_string",
      "account_name": "xxx",
      "container_name": "xxx",
      "base_url":"",
      "secrets": "DefaultEndpointsProtocol=https;AccountName=xxx;AccountKey=xxx;EndpointSuffix=core.windows.net"
    }
  ]
}
```

## TODO

- 使用pickle存储状态续传，blob级续传
- 使用多线程加速 todo
- 添加命令行交互式display
- setting.py 读取配置
  - 数据库名称
  - 时区等

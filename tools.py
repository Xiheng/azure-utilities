units = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB'] # 1EB = (2^10)^6B Z Y B N D C X...
def bytes_unit_convert(size):
    if size < 1024:
        return f"{size}B"

    res = 0.0
    for idx in range(1, len(units)):
        unit = units[idx-1]
        rem = size % 1024
        size = size // 1024
        res = rem+res/1024
        if size == 0:
            break
    if size:
        unit = units[-1]
        res += size
    return f"{res:.2f}{unit}" 

        
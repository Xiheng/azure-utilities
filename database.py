# Build database model and offer some method
from sqlalchemy import Column, create_engine, ForeignKey
from sqlalchemy import Boolean, String, Integer, BigInteger, DateTime
from sqlalchemy.orm import sessionmaker, relationship
from sqlalchemy.ext.declarative import declarative_base
import datetime

from tools import bytes_unit_convert

default_datetime = datetime.datetime(1900,1,1)

DB_name = "test2"
engine = create_engine(f"sqlite+pysqlite:///{DB_name}.db?check_same_thread=False",encoding="utf-8", future=True)

Base =  declarative_base()
Session = sessionmaker(bind=engine)
session = Session()

# Container
# name num_blobs total_size
class Container(Base):
    """
    Table: container
    Cols:
        id
        account_name(required)
        name(requied)
        blobs_count
        total_size
    backrefs:
        folders
        blobs
    """
    __tablename__ = 'container'

    id = Column(Integer, primary_key=True, autoincrement=True)
    account_name = Column(String, nullable=False)
    name = Column(String, nullable=False)
    blobs_count = Column(Integer, default=0)
    total_size = Column(BigInteger, default=0)

    def __repr__(self):
        return f"Container account_name:{self.account_name!r}, name:{self.name!r}"

    @property
    def c_size(self):
        return bytes_unit_convert(total_size)
# Folder
# container path last_modified total_size blobs folders
# list_of_paths list_of_files   is_completed?
class Folder(Base):
    """
    Table: folder
    Cols:
        id
        container_id(required, foreign key)
        path(required)
        size
        mtime
        is_completed
        parent_id(foreign key)
    refs:
        container
        parent
    backrefs:
        subfolders
        blobs
    """
    __tablename__ = 'folder'

    id = Column(Integer, primary_key=True, autoincrement=True)
    container_id = Column(Integer, ForeignKey('container.id'))
    path = Column(String, nullable=False)
    size = Column(BigInteger, default=0)
    mtime = Column(DateTime, default=default_datetime)
    is_completed = Column(Boolean, default=False)
    parent_id = Column(Integer, ForeignKey("folder.id"))

    container = relationship("Container", backref="folders")
    parent = relationship("Folder", remote_side=[id], backref="subfolders")
    
    def __repr__(self):
        return f"Folder path:{self.path!r} size:{self.c_size!r} last_modified:{self.c_mtime!r} is_completed:{self.is_completed!r}"
    
    @property
    def c_mtime(self):
        return self.mtime.strftime("%x %X")

    @property
    def c_size(self):
        return bytes_unit_convert(self.size)

# Blobs
# container path last_modified size 
class Blob(Base):
    """
    Table: blob
    Cols:
        id
        path(required)
        size(required)
        mtime(required)
        container_id(required, foreignkey)
        parent_id(required, foreignkey)
    refs:
        container
        parent
    """
    __tablename__ = 'blob'

    id = Column(Integer, primary_key=True, autoincrement=True)
    path = Column(String, nullable=False)
    size = Column(Integer, default=0)
    mtime = Column(DateTime, default=default_datetime)
    container_id = Column(Integer, ForeignKey('container.id'))
    parent_id = Column(Integer, ForeignKey('folder.id'))

    container = relationship("Container", backref="blobs")
    parent = relationship("Folder", backref="blobs")

    def __repr__(self):
        return f"Blob path:{self.path!r} size:{self.c_size!r} last_modified:{self.c_mtime!r}"
    @property
    def c_mtime(self):
        return self.mtime.strftime("%x %X")

    @property
    def c_size(self):
        return bytes_unit_convert(self.size)

Base.metadata.create_all(engine)

# 插入一些测试数据
# container = Container(account_name="mlmodels", name="retail")
# session.add(container)
# session.flush()
# import datetime
# f0 = Folder(path="/", basename="")
# f1 = Folder(path="/f1/", container_id=1, basename="f1", parent_id=1)

# b1 = Blob(path="/b1", size=4, mtime=datetime.datetime.now(), parent_id=1, container_id=1)
# b2 = Blob(path="/f1/b2", size=8, mtime=datetime.datetime.now(), parent_id=2, container_id=1)
# for i in [f0,b1,f1,b2]:
#     session.add(i)
# session.commit()



def insert_data(sess, cls):
    sess.add(cls)
    sess.flush()
    return cls


def query_insert_data(sess, obj):
    if isinstance(obj, Folder):
        res = sess.query(Folder).filter_by(container_id=obj.container_id, path=obj.path).first()
    elif isinstance(obj, Container):
        res = sess.query(Container).filter_by(account_name=obj.account_name, name=obj.name).first()
    elif isinstance(obj, Blob):
        res = sess.query(Blob).filter_by(container_id=obj.container_id, path=obj.path).first()
        if res:
            res.size = obj.size
            res.mtime = obj.mtime
    else:
        print(obj, " is not legal object!")
        exit(1)
    
    return res if res else insert_data(sess, obj)

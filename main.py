# main
from database import Session
from scan import walk_container, get_container_client
from display import display
import json


SECRET_JSON = "secrets.json"
container_list = json.load(open(SECRET_JSON))['list']


for c in container_list:
    account_name = c['account_name']
    container_name = c['container_name']

    container_client = get_container_client(c['type'], c['account_name'], c['container_name'], c['secrets'], c['base_url'])
    
    prefix = ""
    walk_container(container_client, prefix, resume=True)

    display(account_name, container_name, prefix)
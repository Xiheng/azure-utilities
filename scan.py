from azure.storage.blob import ContainerClient
from database import Session, Container, Folder, Blob
from database import query_insert_data

from tools import bytes_unit_convert


import datetime
import pytz
utc = pytz.UTC

def get_container_client(type, account_name, container_name, secrets, base_url=None):
    if type == "SAS":
        print("TYPE: SAS")
        my_url_sas = base_url+container_name+secrets
        container_client = ContainerClient.from_container_url(my_url_sas)
    else:
        print("TYPE: Connection string")
        container_client = ContainerClient.from_connection_string(secrets, container_name)

    return container_client

def add_subfolder(folder, subfolder):
    folder.size += subfolder.size
    folder.mtime = subfolder.mtime if subfolder.mtime > folder.mtime else folder.mtime

def walk_container(container_client, prefix, resume=True):
    sess = Session()
    account_name = container_client.account_name
    container_name = container_client.container_name
    container = Container(account_name=account_name, name=container_name)
    container = query_insert_data(sess, container)
    if not resume:
        sess.query(Folder).filter_by(container_id=container.id).update({Folder.is_completed: False})
        sess.commit()
    print(f'C: {account_name}/{container_name}/{prefix}')
    depth = 1
    separator = '   '
    stk = []
    prefixs = []
    def walk(prefix, cid):
        nonlocal depth
        prefixs.append(prefix)
        stk.append(container_client.walk_blobs(name_starts_with=prefix))
        folder = Folder(container_id=cid, path=prefix)
        folder = query_insert_data(sess, folder)
        if folder.is_completed:
            print("All done. Skipping...")
            return
        else:
            folder.size = 0
        print( datetime.datetime.now().strftime("%x %X"), f'F: {prefix}')
        while stk:
            message = datetime.datetime.now().strftime("%x %X ")
            try:
                item = stk[-1].next()
                short_name = item.name[len(prefixs[-1]):]
                
                if item.name[-1] == '/':
                    message += 'F: ' + separator * depth + short_name
                    subfolder = Folder(container_id=cid, path=item.name, parent_id=folder.id)
                    subfolder = query_insert_data(sess, subfolder)
                    if subfolder.is_completed:
                        add_subfolder(folder, subfolder)
                        print(message, "done, skipping")
                        continue
                    else:
                        subfolder.size = 0
                    print(message)
                    depth += 1
                    prefixs.append(item.name)
                    parent_id = subfolder.id
                    folder = subfolder
                    stk.append(container_client.walk_blobs(name_starts_with=item.name))
                else:
                    message += 'B: '  + separator * depth + short_name
                    results = list(container_client.list_blobs(name_starts_with=item.name, include=['snapshots']))
                    num_snapshots = len(results) - 1
                    
                    size = item.size
                    mtime = item.last_modified.astimezone(utc).replace(tzinfo=None)
                    message += f" {mtime}"
                    if num_snapshots:
                        message += " ({} snapshots)".format(num_snapshots)
                        size = 0
                        for snap in results: size += snap.size
                        message += f" {bytes_unit_convert(size)}"
                    else:
                        message +=  f" {bytes_unit_convert(size)}"
                    blob = Blob(path=item.name, size=size, mtime=mtime, container_id=cid, parent_id=folder.id)
                    blob = query_insert_data(sess, blob)
                    blob.parent.size += size
                    print(message)
                    folder.mtime = blob.mtime if blob.mtime > folder.mtime else folder.mtime
                    blob.container.blobs_count += 1
                    

            except StopIteration:
                stk.pop()
                prefixs.pop()
                depth -= 1
                folder.is_completed = True
                if folder.parent:
                    subfolder = folder
                    folder = subfolder.parent
                    add_subfolder(folder, subfolder)
                sess.commit()
                    
    walk(prefix, container.id)
    container.total_size = sess.query(Folder.size).filter_by(container_id=container.id, path=prefix).first()[0]
    sess.commit()
